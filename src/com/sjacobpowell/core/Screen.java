package com.sjacobpowell.core;

import java.util.Set;
import java.util.function.Consumer;

import com.sjacobpowell.puzzle.Line;

public class Screen extends Bitmap {
	private Bitmap fixedNumberBackground = null;
	private Bitmap[] numberSprites;
	private Bitmap[] numberSpritesTiny;

	public Screen(int width, int height) {
		super(width, height);
		numberSprites = new Bitmap[9];
		numberSpritesTiny = new Bitmap[9];
		for (int i = 0; i < numberSprites.length; i++) {
			numberSprites[i] = new Bitmap(32, 32);
			numberSprites[i].draw(ArtTools.getScaledBitmap("/numbers/" + (i + 1) + ".png", 28, 28), 2, 2);
			for (int j = 0; j < numberSprites[i].pixels.length; j++) {
				if (numberSprites[i].pixels[j] != 0) {
					numberSprites[i].pixels[j] = 0x49976B;
				}
			}
			numberSpritesTiny[i] = ArtTools.getScaledBitmap("/numbers/" + (i + 1) + ".png", 10, 10);
			for (int j = 0; j < numberSpritesTiny[i].pixels.length; j++) {
				if (numberSpritesTiny[i].pixels[j] != 0) {
					numberSpritesTiny[i].pixels[j] = 0x0E5A2F;
				}
			}
		}
	}

	public void render(Game game) {
		if (fixedNumberBackground == null) {
			fixedNumberBackground = createNumberBackground(game);
		}
		clear();
		draw(game.cursor.sprite, game.cursor.leftEdge(), game.cursor.topEdge());
		drawNumbers(game);
		Consumer<Line> lineConsumer = line -> draw(line.sprite, line.leftEdge(), line.topEdge());
		game.lines.verticalLines.stream().filter(line -> !line.thick).forEach(lineConsumer);
		game.lines.horizontalLines.stream().filter(line -> !line.thick).forEach(lineConsumer);
		game.lines.verticalLines.stream().filter(line -> line.thick).forEach(lineConsumer);
		game.lines.horizontalLines.stream().filter(line -> line.thick).forEach(lineConsumer);
		if (game.solver.solving) {
			for (int row = 0; row < game.solver.rowCount; row++) {
				for (int column = 0; column < game.solver.columnCount; column++) {
					double xOffset = game.lines.verticalLines.get(column).rightEdge();
					double yOffset = game.lines.horizontalLines.get(row).bottomEdge();
					int i = 0;
					Set<Integer> candidates = game.solver.getCandidatesCopyAt(row, column);
					if (candidates != null && candidates.size() > 1 && game.puzzle.numberGrid[row][column] == 0) {
						for (int candidate : candidates) {
							draw(numberSpritesTiny[candidate - 1], xOffset + 10 * (i % 3), yOffset + 10 * (i / 3));
							i++;
						}
					}
				}
			}
		}
	}

	private Bitmap createNumberBackground(Game game) {
		Bitmap puzzleNumber = new Bitmap(game.cursor.sprite.width, game.cursor.sprite.height);
		for (int i = 0; i < puzzleNumber.pixels.length; i++) {
			puzzleNumber.pixels[i] = 0x3F423B;
		}
		return puzzleNumber;
	}

	private void drawNumbers(Game game) {
		Bitmap numberSprite;
		double x;
		double y;
		for (int row = 0; row < 9; row++) {
			for (int column = 0; column < 9; column++) {
				if (game.puzzle.numberGrid[row][column] > 0) {
					numberSprite = numberSprites[game.puzzle.numberGrid[row][column] - 1];
					x = game.lines.verticalLines.get(column).rightEdge();
					y = game.lines.horizontalLines.get(row).bottomEdge();
					if (game.puzzle.fixedNumbers[row][column] && !(game.row == row && game.column == column)) {
						draw(fixedNumberBackground, x, y);
					}
					draw(numberSprite, x, y);
				}
			}
		}
	}

}
