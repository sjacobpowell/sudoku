package com.sjacobpowell.core;

import java.awt.Point;

import javax.swing.JOptionPane;

import com.sjacobpowell.Controller;
import com.sjacobpowell.puzzle.Cursor;
import com.sjacobpowell.puzzle.Lines;
import com.sjacobpowell.puzzle.Puzzle;
import com.sjacobpowell.solver.Solver;
import com.sun.glass.events.KeyEvent;

public class Game {
	public int time;
	public int width;
	public int height;
	public int scale;
	public Cursor cursor;
	public int row = 0;
	public int column = 0;
	public Solver solver;
	public Lines lines;
	public Puzzle puzzle;
	private boolean won;
	private boolean[] numbersPressed = new boolean[10];
	private Controller controller;

	public Game(int width, int height, int scale) {
		this.width = width;
		this.height = height;
		this.scale = scale;
		puzzle = new Puzzle();
		clearPuzzle();
		controller = new Controller(this);
		cursor = new Cursor();
		lines = new Lines(width, height, cursor.sprite.width, cursor.sprite.height);
		controller.setVisible(true);
	}

	public void clearPuzzle() {
		puzzle.clear();
		won = false;
		if(solver != null) {
			solver.solving = false;
		}
		solver = new Solver(puzzle.numberGrid);
	}

	public void tick(InputManager inputManager) {
		if (won) {
			return;
		}
		numbersPressed[1] = inputManager.keys[KeyEvent.VK_1] || inputManager.keys[KeyEvent.VK_NUMPAD1];
		numbersPressed[2] = inputManager.keys[KeyEvent.VK_2] || inputManager.keys[KeyEvent.VK_NUMPAD2];
		numbersPressed[3] = inputManager.keys[KeyEvent.VK_3] || inputManager.keys[KeyEvent.VK_NUMPAD3];
		numbersPressed[4] = inputManager.keys[KeyEvent.VK_4] || inputManager.keys[KeyEvent.VK_NUMPAD4];
		numbersPressed[5] = inputManager.keys[KeyEvent.VK_5] || inputManager.keys[KeyEvent.VK_NUMPAD5];
		numbersPressed[6] = inputManager.keys[KeyEvent.VK_6] || inputManager.keys[KeyEvent.VK_NUMPAD6];
		numbersPressed[7] = inputManager.keys[KeyEvent.VK_7] || inputManager.keys[KeyEvent.VK_NUMPAD7];
		numbersPressed[8] = inputManager.keys[KeyEvent.VK_8] || inputManager.keys[KeyEvent.VK_NUMPAD8];
		numbersPressed[9] = inputManager.keys[KeyEvent.VK_9] || inputManager.keys[KeyEvent.VK_NUMPAD9];
		boolean up = inputManager.keys[KeyEvent.VK_UP] || inputManager.keys[KeyEvent.VK_K];
		boolean down = inputManager.keys[KeyEvent.VK_DOWN] || inputManager.keys[KeyEvent.VK_ENTER]
				|| inputManager.keys[KeyEvent.VK_J];
		boolean left = inputManager.keys[KeyEvent.VK_LEFT] || inputManager.keys[KeyEvent.VK_H];
		boolean right = inputManager.keys[KeyEvent.VK_RIGHT] || inputManager.keys[KeyEvent.VK_L];

		if (solver.solved) {
			checkWin();
		}
		if (solver.solving) {
			cursor.x = lines.verticalLines.get(solver.column).rightEdge() + cursor.sprite.width / 2;
			cursor.y = lines.horizontalLines.get(solver.row).bottomEdge() + cursor.sprite.height / 2;
		} else {
			if (inputManager.clicked != null) {
				moveCursor(inputManager.clicked);
			}
			moveCursor(up, down, left, right, inputManager);

			for (int i = 0; i < numbersPressed.length; i++) {
				if (numbersPressed[i]) {
					enterNumber();
					checkWin();
					break;
				}
			}
			if (inputManager.keys[KeyEvent.VK_SPACE] || inputManager.keys[KeyEvent.VK_0]
					|| inputManager.keys[KeyEvent.VK_NUMPAD0] || inputManager.keys[KeyEvent.VK_BACKSPACE]
					|| inputManager.keys[KeyEvent.VK_DELETE] || inputManager.keys[KeyEvent.VK_MINUS]
					|| inputManager.keys[KeyEvent.VK_SUBTRACT]) {
				puzzle.numberGrid[row][column] = 0;
			}
			for (int i = 0; i < inputManager.keys.length; i++) {
				inputManager.keys[i] = false;
			}
		}

		time++;
	}

	private void enterNumber() {
		for (int i = 0; i < numbersPressed.length; i++) {
			if (numbersPressed[i] && !puzzle.fixedNumbers[row][column]) {
				puzzle.numberGrid[row][column] = i;
				return;
			}
		}
	}

	private void checkWin() {
		if (puzzle.isSolved()) {
			new Thread(
					() -> JOptionPane.showMessageDialog(null, "You win!", "You win!", JOptionPane.INFORMATION_MESSAGE))
							.start();
			won = true;
		}

	}

	private void moveCursor(boolean up, boolean down, boolean left, boolean right, InputManager inputManager) {
		if (up || down) {
			row = up ? row - 1 : row + 1;
		}

		if (left || right) {
			column = left ? column - 1 : column + 1;
		}

		row = row < 0 ? 0 : row;
		column = column < 0 ? 0 : column;
		row = row > 8 ? 8 : row;
		column = column > 8 ? 8 : column;
		cursor.x = lines.verticalLines.get(column).rightEdge() + cursor.sprite.width / 2;
		cursor.y = lines.horizontalLines.get(row).bottomEdge() + cursor.sprite.height / 2;
		inputManager.clicked = null;
	}

	private void moveCursor(Point clicked) {
		for (int i = 1; i < lines.verticalLines.size(); i++) {
			if (lines.verticalLines.get(i).rightEdge() * scale > clicked.getX()) {
				column = i - 1;
				break;
			}
		}

		for (int i = 1; i < lines.horizontalLines.size(); i++) {
			if (lines.horizontalLines.get(i).bottomEdge() * scale > clicked.getY()) {
				row = i - 1;
				break;
			}
		}
	}
}
