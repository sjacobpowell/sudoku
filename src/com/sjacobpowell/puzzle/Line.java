package com.sjacobpowell.puzzle;

import com.sjacobpowell.core.Bitmap;
import com.sjacobpowell.core.Entity;

public class Line extends Entity {
	public boolean thick;
	public Line(int length, boolean vertical, boolean thick) {
		this.thick = thick;
		sprite = new Bitmap(vertical ? (thick ? 4 : 1) : length, vertical ? length : (thick ? 4 : 1));
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = thick ? 0x022A35 : 0x01181F;
		}
		x = sprite.width / 2;
		y = sprite.height / 2;
	}
}
