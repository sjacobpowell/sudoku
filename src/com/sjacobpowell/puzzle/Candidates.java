package com.sjacobpowell.puzzle;

import java.util.Set;
import java.util.TreeSet;

public class Candidates {
	public Set<Integer> set;
	
	public Candidates() {
		this.set = new TreeSet<Integer>();
	}
	
	public Candidates(Set<Integer> set) {
		this.set = set;
	}
}
