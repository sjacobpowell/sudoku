package com.sjacobpowell.puzzle;

import com.sjacobpowell.core.Bitmap;
import com.sjacobpowell.core.Entity;

public class Cursor extends Entity {
	private int color = 0x450027;
	
	public Cursor() {
		init();
	}

	public Cursor(int color) {
		this.color = color;
		init();
	}
	
	private void init() {
		sprite = new Bitmap(32, 32);
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = color;
		}
	}

}
