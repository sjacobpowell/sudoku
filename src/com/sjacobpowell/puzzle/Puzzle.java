package com.sjacobpowell.puzzle;

public class Puzzle {
	public int[][] numberGrid;
	public boolean[][] fixedNumbers;

	private final static int[][] originalPuzzle = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0 } };
	private int[][] solution = {
			{ 8, 1, 2, 7, 5, 3, 6, 4, 9 },
			{ 9, 4, 3, 6, 8, 2, 1, 7, 5 },
			{ 6, 7, 5, 4, 9, 1, 2, 8, 3 },
			{ 1, 5, 4, 2, 3, 7, 8, 9, 6 },
			{ 3, 6, 9, 8, 4, 5, 7, 2, 1 },
			{ 2, 8, 7, 1, 6, 9, 5, 3, 4 },
			{ 5, 2, 1, 9, 7, 4, 3, 6, 8 },
			{ 4, 3, 8, 5, 2, 6, 9, 1, 7 },
			{ 7, 9, 6, 3, 1, 8, 4, 5, 2 } };
	
	public Puzzle() {
		clear();
		fixedNumbers = new boolean[numberGrid.length][];
		for (int row = 0; row < fixedNumbers.length; row++) {
			fixedNumbers[row] = new boolean[numberGrid[row].length];
			for (int column = 0; column < fixedNumbers[0].length; column++) {
				fixedNumbers[row][column] = numberGrid[row][column] != 0;
			}
		}
	}

	public void clear() {
		numberGrid = new int[originalPuzzle.length][];
		for (int row = 0; row < 9; row++) {
			numberGrid[row] = new int[originalPuzzle[row].length];
			for (int column = 0; column < 9; column++) {
				numberGrid[row][column] = originalPuzzle[row][column];
			}
		}
	}

	public boolean isSolved() {
		for (int row = 0; row < 9; row++) {
			for (int column = 0; column < 9; column++) {
				if (numberGrid[row][column] != solution[row][column]) {
					return false;
				}
			}
		}
		return true;
	}

}
