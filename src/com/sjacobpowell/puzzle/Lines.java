package com.sjacobpowell.puzzle;

import java.util.ArrayList;
import java.util.List;

public class Lines {
	public List<Line> verticalLines;
	public List<Line> horizontalLines;
	
	public Lines(int width, int height, int cursorWidth, int cursorHeight) {
		verticalLines = new ArrayList<Line>();
		horizontalLines = new ArrayList<Line>();
		boolean thick;
		Line verticalLine;
		Line horizontalLine;
		for (int i = 0; i < 10; i++) {
			thick = i == 0 || i == 3 || i == 6 || i == 9;
			verticalLine = new Line(height, true, thick);
			horizontalLine = new Line(width, false, thick);
			if (i > 0) {
				verticalLine.x = verticalLines.get(i - 1).rightEdge() + cursorWidth;
				horizontalLine.y = horizontalLines.get(i - 1).bottomEdge() + cursorHeight;
			}
			verticalLines.add(verticalLine);
			horizontalLines.add(horizontalLine);
		}
	}
}
