package com.sjacobpowell;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.sjacobpowell.core.Game;

public class Controller extends JFrame {
	private static final long serialVersionUID = 1L;
	JButton clear = new JButton("Clear");
	JButton solve = new JButton("Solve");
	JButton delayedSolve = new JButton("Delayed Solve");
	private Game game;

	public Controller(Game game) {
		super("Sudoku Controller");
		this.game = game;
		Dimension size = new Dimension(298, 298);
		setSize(size);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setResizable(false);
		setLayout(new GridLayout(3, 1));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setActionListeners();
		addControls();
		pack();
	}

	private void setActionListeners() {
		clear.addActionListener(e -> {
			game.clearPuzzle();
			solve.setEnabled(true);
			delayedSolve.setEnabled(true);
		});
		solve.addActionListener(e -> {
			solve.setEnabled(false);
			delayedSolve.setEnabled(false);
			game.clearPuzzle();
			game.solver.solve(0);
		});
		delayedSolve.addActionListener(e -> {
			solve.setEnabled(false);
			delayedSolve.setEnabled(false);
			game.clearPuzzle();
			game.solver.solve(100);
		});
	}

	private void addControls() {
		add(clear);
		add(solve);
		add(delayedSolve);
	}

}
