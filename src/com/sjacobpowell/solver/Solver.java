package com.sjacobpowell.solver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

import com.sjacobpowell.puzzle.Candidates;

public class Solver {
	private int[][] puzzle;
	private Eliminator eliminator;
	private Candidates[][] cellCandidates;
	private static Semaphore[][] locks;
	private List<Set<Integer>> squareCandidates;
	private int milliseconds = 0;
	public int rowCount;
	public int columnCount;
	public boolean solving;
	public boolean solved;
	public int column = 0;
	public int row = 0;

	public Solver(int[][] puzzle) {
		solving = false;
		solved = false;
		this.puzzle = puzzle;
		rowCount = puzzle.length;
		columnCount = puzzle[0].length;
		locks = new Semaphore[rowCount][];
		for (int row = 0; row < rowCount; row++) {
			locks[row] = new Semaphore[columnCount];
			for (int column = 0; column < columnCount; column++) {
				locks[row][column] = new Semaphore(1, true);
			}
		}
	}

	private void acquireMutex(int row, int column) {
		try {
			locks[row][column].acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void releaseMutex(int row, int column) {
		locks[row][column].release();
	}

	public Set<Integer> getCandidatesCopyAt(int row, int column) {
		Set<Integer> set = null;
		try {
			locks[row][column].acquire();
			set = cellCandidates[row][column].set.stream().collect(Collectors.toSet());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		locks[row][column].release();
		if (!solving) {
			return null;
		} else {
			return set;
		}
	}

	public void solve(int milliseconds) {
		this.milliseconds = milliseconds;
		new Thread(() -> {
			long start = System.nanoTime();
			solvePuzzle();
			long end = System.nanoTime();
			System.err.println("Solve was " + (solved ? "successful" : "unsuccessful") + ".");
			if (milliseconds <= 0) {
				long millis = (end - start) / 1000000;
				long seconds = millis / 1000;
				System.err.println("The computer took " + millis + " milliseconds, (" + seconds
						+ " seconds), to solve the puzzle.");
			}
		}).start();
	}

	private void solvePuzzle() {
		cellCandidates = new Candidates[rowCount][];
		for (int row = 0; row < cellCandidates.length; row++) {
			cellCandidates[row] = new Candidates[columnCount];
			for (int column = 0; column < cellCandidates[row].length; column++) {
				acquireMutex(row, column);
				cellCandidates[row][column] = new Candidates();
				releaseMutex(row, column);
			}
		}
		eliminator = new Eliminator(cellCandidates);
		solving = true;
		solved = false;
		if (!simpleSolve()) {
			solved = backtrack() && solving;
		}
		solving = false;
	}

	private boolean backtrack() {
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				if (backtrackRecursive(row, column)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean backtrackRecursive(int row, int column) {
		Set<Integer> candidates = getCandidatesCopyAt(row, column);
		if (puzzle[row][column] != 0 || candidates == null || candidates.size() == 0) {
			return false;
		}
		this.row = row;
		this.column = column;
		final int[][] backupPuzzle = getPuzzleDeepCopy();
		for (int current : candidates) {
			puzzle[row][column] = current;
			updateSquareCandidates();
			updateCandidates();
			if (simpleSolve()) {
				return true;
			} else {
				column += 1;
				if (column > 8) {
					column = 0;
					row += 1;
				}
				row = row > 8 ? 0 : row;
				if (backtrackRecursive(row, column)) {
					return true;
				} else {
					restorePuzzle(backupPuzzle);
				}
			}
			sleep();
		}
		return false;
	}

	private void restorePuzzle(int[][] backupPuzzle) {
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				puzzle[row][column] = backupPuzzle[row][column];
			}
		}
	}

	private int[][] getPuzzleDeepCopy() {
		int[][] backupPuzzle = new int[rowCount][];
		for (int row = 0; row < rowCount; row++) {
			backupPuzzle[row] = new int[columnCount];
			for (int column = 0; column < columnCount; column++) {
				backupPuzzle[row][column] = puzzle[row][column];
			}
		}
		return backupPuzzle;
	}

	private boolean simpleSolve() {
		initializeCandidates();
		boolean allSolutionsEntered;
		boolean changed;
		do {
			allSolutionsEntered = true;
			changed = false;
			for (int row = 0; row < rowCount; row++) {
				for (int column = 0; column < columnCount; column++) {
					this.row = row;
					this.column = column;
					acquireMutex(row, column);
					changed |= eliminator.eliminateCandidates(row, column, cellCandidates[row][column].set);
					releaseMutex(row, column);
					updateCandidates();
					acquireMutex(row, column);
					allSolutionsEntered &= enterSolution(row, column);
					releaseMutex(row, column);
					sleep();
				}
			}
		} while (changed && solving);
		return allSolutionsEntered;
	}

	private boolean enterSolution(int row, int column) {
		if (cellCandidates[row][column].set.size() == 1) {
			puzzle[row][column] = cellCandidates[row][column].set.iterator().next();
			sleep();
			return true;
		}
		return false;
	}

	private void updateCandidates() {
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				for (int cellCandidate : squareCandidates.get((column / 3) + (row / 3) * 3)) {
					if (!absentOnRow(cellCandidate, row, column) || !absentOnColumn(cellCandidate, row, column)) {
						acquireMutex(row, column);
						cellCandidates[row][column].set.remove(cellCandidate);
						releaseMutex(row, column);
					}
				}
			}
		}
	}

	private void sleep() {
		if (milliseconds <= 0 || !solving) {
			return;
		}

		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void initializeCandidates() {
		squareCandidates = createSquareCandidates();
		for (int row = 0; row < rowCount; row++) {
			for (int column = 0; column < columnCount; column++) {
				if (puzzle[row][column] != 0) {
					acquireMutex(row, column);
					cellCandidates[row][column].set.add(puzzle[row][column]);
					releaseMutex(row, column);
				} else {
					for (int cellCandidate : squareCandidates.get((column / 3) + (row / 3) * 3)) {
						if (absentOnRow(cellCandidate, row, column) && absentOnColumn(cellCandidate, row, column)) {
							acquireMutex(row, column);
							cellCandidates[row][column].set.add(cellCandidate);
							releaseMutex(row, column);
						}
					}
				}
			}
		}
	}

	private boolean absentOnRow(int cellCandidate, int row, int column) {
		for (int currentColumn = 0; currentColumn < columnCount; currentColumn++) {
			if (currentColumn != column && puzzle[row][currentColumn] == cellCandidate) {
				return false;
			}
		}
		return true;
	}

	private boolean absentOnColumn(int cellCandidate, int row, int column) {
		for (int currentRow = 0; currentRow < rowCount; currentRow++) {
			if (currentRow != row && puzzle[currentRow][column] == cellCandidate) {
				return false;
			}
		}
		return true;
	}

	private List<Set<Integer>> createSquareCandidates() {
		List<Integer> possibleNumbers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

		List<Set<Integer>> squareCandidates = new ArrayList<Set<Integer>>();
		for (int i = 0; i < 9; i++) {
			squareCandidates.add(new TreeSet<Integer>(possibleNumbers));
		}

		for (int square = 0; square < squareCandidates.size(); square++) {
			int squareRow = (square / 3);
			int squareColumn = (square % 3);
			for (int row = 0 + 3 * squareRow; row < 3 + 3 * squareRow; row++) {
				for (int column = 0 + 3 * squareColumn; column < 3 + 3 * squareColumn; column++) {
					if (puzzle[row][column] != 0) {
						squareCandidates.get(square).remove(puzzle[row][column]);
					}
				}
			}
		}
		return squareCandidates;
	}
	
	private void updateSquareCandidates() {
		for (int square = 0; square < squareCandidates.size(); square++) {
			int squareRow = (square / 3);
			int squareColumn = (square % 3);
			for (int row = 0 + 3 * squareRow; row < 3 + 3 * squareRow; row++) {
				for (int column = 0 + 3 * squareColumn; column < 3 + 3 * squareColumn; column++) {
					if (puzzle[row][column] != 0) {
						squareCandidates.get(square).remove(puzzle[row][column]);
					}
				}
			}
		}
	}

}
