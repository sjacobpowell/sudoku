package com.sjacobpowell.solver;

import java.util.Iterator;
import java.util.Set;

import com.sjacobpowell.puzzle.Candidates;

public class Eliminator {
	private Candidates[][] cellCandidates;
	
	public Eliminator(Candidates[][] cellCandidates) {
		this.cellCandidates = cellCandidates;
	}
	
	public boolean eliminateCandidates(int row, int column, Set<Integer> candidates) {
		if (candidates.size() == 1) {
			return false;
		}
		boolean changed = false;
		changed |= eliminateInSquare(row, column, candidates);
		changed |= eliminateInRow(row, column, candidates);
		changed |= eliminateInColumn(row, column, candidates);
		if (candidates.size() == 2) {
			changed |= eliminateNakedTwinsSquare(row, column, candidates);
			changed |= eliminateNakedTwinsRowAndColumn(row, column, candidates);
		}
		return changed;
	}

	public boolean eliminateNakedTwinsSquare(int row, int column, Set<Integer> candidates) {
		int twinRow = 0;
		int twinColumn = 0;
		int squareRowStart = (row / 3) * 3;
		int squareColumnStart = (column / 3) * 3;
		for (int squareRow = squareRowStart; squareRow < squareRowStart + 3; squareRow++) {
			for (int squareColumn = squareColumnStart; squareColumn < squareColumnStart + 3; squareColumn++) {
				if ((squareRow != row || squareColumn != column)
						&& candidates.equals(cellCandidates[squareRow][squareColumn].set)) {
					twinRow = squareRow;
					twinColumn = squareColumn;
					break;
				}
			}
		}
		boolean changed = false;
		if (twinRow != 0 && twinColumn != 0) {
			for (int squareRow = squareRowStart; squareRow < squareRowStart + 3; squareRow++) {
				for (int squareColumn = squareColumnStart; squareColumn < squareColumnStart + 3; squareColumn++) {
					if ((squareRow != row || squareColumn != column)
							&& (squareRow != twinRow || squareColumn != twinColumn)) {
						changed |= cellCandidates[squareRow][squareColumn].set.removeAll(candidates);
					}
				}
			}
		}
		return changed;
	}

	public boolean eliminateNakedTwinsRowAndColumn(int row, int column, Set<Integer> candidates) {
		boolean sameRow = true;
		int rowIndex = 0;
		int columnIndex = 0;
		for (int i = 0; i < cellCandidates.length; i++) {
			if (i != row) {
				if (candidates.equals(cellCandidates[i][column].set)) {
					sameRow = false;
					rowIndex = i;
					break;
				}
			}

			if (i != column) {
				if (candidates.equals(cellCandidates[row][i].set)) {
					sameRow = true;
					columnIndex = i;
					break;
				}
			}
		}

		boolean changed = false;
		if (rowIndex != 0 || columnIndex != 0) {
			for (int i = 0; i < cellCandidates.length; i++) {
				if (sameRow) {
					if (i != column && i != columnIndex) {
						changed |= cellCandidates[row][i].set.removeAll(candidates);
					}
				} else {
					if (i != row && i != rowIndex) {
						changed |= cellCandidates[i][column].set.removeAll(candidates);
					}
				}
			}
		}
		return changed;
	}

	public boolean eliminateInSquare(int row, int column, Set<Integer> candidates) {
		int current = 0;
		boolean alone = false;
		for (Iterator<Integer> it = candidates.iterator(); it.hasNext();) {
			alone = true;
			current = it.next();
			int squareRowStart = (row / 3) * 3;
			int squareColumnStart = (column / 3) * 3;
			for (int squareRow = squareRowStart; squareRow < squareRowStart + 3; squareRow++) {
				for (int squareColumn = squareColumnStart; squareColumn < squareColumnStart + 3; squareColumn++) {
					if ((squareRow != row || squareColumn != column)
							&& cellCandidates[squareRow][squareColumn].set.contains(current)) {
						alone = false;
						break;
					}
				}
				if (!alone) {
					break;
				}
			}
			if (alone) {
				break;
			}
		}
		if (alone) {
			stripToValue(current, candidates);
		}
		return alone;
	}

	public boolean eliminateInRow(int row, int column, Set<Integer> candidates) {
		int current = 0;
		boolean alone = false;
		for (Iterator<Integer> it = candidates.iterator(); it.hasNext();) {
			alone = true;
			current = it.next();

			for (int currentColumn = 0; currentColumn < cellCandidates[row].length; currentColumn++) {
				if (currentColumn != column && cellCandidates[row][currentColumn].set.contains(current)) {
					alone = false;
					break;
				}
			}

			if (alone) {
				break;
			}
		}
		if (alone) {
			stripToValue(current, candidates);
		}
		return alone;
	}

	public boolean eliminateInColumn(int row, int column, Set<Integer> candidates) {
		int current = 0;
		boolean alone = false;
		for (Iterator<Integer> it = candidates.iterator(); it.hasNext();) {
			alone = true;
			current = it.next();

			for (int currentRow = 0; currentRow < cellCandidates.length; currentRow++) {
				if (currentRow != row && cellCandidates[currentRow][column].set.contains(current)) {
					alone = false;
					break;
				}
			}

			if (alone) {
				break;
			}
		}
		if (alone) {
			stripToValue(current, candidates);
		}
		return alone;
	}

	public void stripToValue(int current, Set<Integer> candidates) {
		if (current == 0) {
			return;
		}
		candidates.clear();
		candidates.add(current);
	}

}
